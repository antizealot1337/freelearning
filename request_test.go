package freelearning

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"golang.org/x/net/html"
)

func TestMakeRequest(t *testing.T) {
	status := http.StatusNotFound

	// Start an http server
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(status)
	}))

	// Make a request to the server
	resp, err := makeRequest(srv.URL)

	// Check for an error
	if err == nil {
		t.Error("Expected error from status 404")
	} //if

	// Make sure the response is nil
	if resp != nil {
		t.Error("Expected reponse to be nil")
	} //if

	// Change the status
	status = http.StatusOK

	// Make a request to the server
	resp, err = makeRequest(srv.URL)

	// Make sure the error is nil
	if err != nil {
		t.Error("Expecter error to be nil")
	} //if

	// Make sure the response isn't nil
	if resp == nil {
		t.Errorf("Expected the response to not be nil")
	} //if
} //func

func TestParseBookInfo(t *testing.T) {
	// Parse an empty string
	_, _, err := parseBookInfo(bytes.NewBuffer(nil))

	// Check for an error
	if err == nil {
		t.Error("Expected error from empty reader")
	} //if

	// Open the file containint HTML
	f, err := os.Open("testdata/free-learning.html")

	// Check for an error
	if err != nil {
		panic(err)
	} //if

	// Make sure the file is closed
	defer f.Close()

	// Set the title of the eBook in the testing HTML
	title :=
		"Building Virtual Pentesting Labs for Advanced Penetration Testing"

	cover := "https://d255esdrn735hr.cloudfront.net/sites/default/files/imagecache/dotd_main_image/4771OS.jpg"

	// Parse the free learning HTML
	book, img, err := parseBookInfo(f)

	// Check the error
	if err != nil {
		// t.Fatal("Unexpected error:", err)
	} //if

	// Check the title
	if book != title {
		t.Errorf(`Expected book to be "%s" but was "%s"`, title, book)
	} //if

	// Check the image
	if img != cover {
		t.Errorf(`Expected image url to be "%s" but was "%s"`, cover, img)
	} //if
} //func

func TestAttrMap(t *testing.T) {
	// Create a buffer for the tokenizer
	in := bytes.NewBufferString(`<html><body><img class="test"></img></body></html>`)

	// Create a tokenizer
	tok := html.NewTokenizer(in)

	// Get the next token
	tok.Next()
	tok.Next()
	tok.Next()

	// Get the attrs
	attrs := attrMap(tok)

	// Check the size of the map
	if len(attrs) == 0 {
		t.Fatal("Expected attrs in the map but was empty")
	} //if

	// Check the class attr
	if class, ok := attrs["class"]; !ok {
		t.Error("Expected attributes for class")
	} else if expected, actual := "test", class; actual != expected {
		t.Errorf(`Expected the class to be "%s" but was "%s"`, expected, actual)
	} //if
} //func
