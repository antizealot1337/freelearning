import httpclient
import htmlparser
import strutils
import streams
import xmltree

const
  page = "https://www.packtpub.com/packt/offers/free-learning"

proc getFreeBook*(): string =
  let page = parseHtml(newStringStream(page.getContent()))

  for n in page.findAll("h2"):
    return n.innerText.strip

when isMainModule:
  let freebook = getFreeBook()
  if freebook == "":
    quit("An error occured attempting to fetch the free ebook", 1)
  else:
    echo getFreeBook()
