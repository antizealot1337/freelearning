package freelearning

import (
	"errors"
	"io"
	"net/http"
	"strings"

	"golang.org/x/net/html"
)

const (
	freelearningURL = "https://www.packtpub.com/packt/offers/free-learning"
)

// GetFreeEBook attempts to retrieve and parse the free eBook from Packtpub.
func GetFreeEBook() (title, img string, err error) {
	// Make the request
	r, err := makeRequest(freelearningURL)

	// Make sure there was no error
	if err != nil {
		return "", "", err
	} //if

	// Make sure the read closer is closed at the end of the function
	defer r.Close()

	return parseBookInfo(r)
} //func

func makeRequest(address string) (io.ReadCloser, error) {
	// Make the request
	req, err := http.Get(address)
	if err != nil {
		return nil, err
	} //if

	// Make sure the status is "OK"
	if req.StatusCode != http.StatusOK {
		// Close the request now
		req.Body.Close()

		// Return the status error
		return nil, errors.New(req.Status)
	} //if

	return req.Body, nil
} //func

func parseBookInfo(in io.Reader) (title, img string, err error) {
	// Create a tokenizer
	tok := html.NewTokenizer(in)

	// Flag that determines if the tag containint the book title has been found
	var titleFound bool

	// The loop that tokenizes the input
	for {
		// Get the next token
		t := tok.Next()

		// Determine the token type
		switch t {
		case html.ErrorToken:
			err = tok.Err()

			// End the loop
			return
		case html.TextToken:
			// Check if the tag with the title has been found
			if titleFound {
				// Set the title
				title = strings.TrimSpace(string(tok.Text()))

				// Set title found to false
				titleFound = false
			} //if
		case html.StartTagToken, html.SelfClosingTagToken:
			// Get the name of the tag
			tag, _ := tok.TagName()

			// Check the tag
			switch stag := string(tag); stag {
			case "h2":
				if title == "" {
					titleFound = true
				} //if
			case "img":
				attrs := attrMap(tok)

				// There is a gif image that has the original as a "data-" attribute
				if src, ok := attrs["data-original"]; ok {
					img = "https:" + src
				} //if
			} //switch
		case html.EndTagToken:
		default:
		} //switch

		// Check if the title and image have been set
		if title != "" && img != "" {
			return
		} //if
	} //for
} //func

func attrMap(tok *html.Tokenizer) map[string]string {
	// Create a map for the attrs
	attrs := make(map[string]string)

	// Parse the attirbutes
end:
	for {
		// Get the next attribute
		key, val, more := tok.TagAttr()

		// Add to the map
		attrs[string(key)] = string(val)

		// Check if there are more
		if !more {
			break end
		} //if
	} //for

	// Return the attrs
	return attrs
} //attrMap
