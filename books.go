package freelearning

import (
	"bytes"
	"io"
	"strings"
)

// Books is the database of books in the Packtpub library.
type Books struct {
	books []string
} //struct

// ParseBooks parses a Reader for books.
func ParseBooks(in io.Reader) (*Books, error) {
	var books []string

	// Start looping
	for {
		// Read a line
		line, err := parseLine(in)

		// Check if the line is not an empty string
		if line != "" {
			// Add the book
			books = append(books, line)
		} //if

		// Check the error
		if err == nil {
			continue
		} else if err == io.EOF {
			break
		} //if

		// Return the error
		return nil, err
	} //for

	// Return the books
	return &Books{books: books}, nil
} //parseBooks

func parseLine(in io.Reader) (string, error) {
	var buff bytes.Buffer
	bite := make([]byte, 1)

	// Read the first bite
	if _, err := in.Read(bite); err != nil {
		return "", err
	} //if

	// Start looping
	for bite[0] != '\n' {
		// Write what has been read alreayd
		buff.Write(bite)

		// Read the next byte
		if _, err := in.Read(bite); err != nil {
			return strings.TrimSpace(buff.String()), err
		} //if
	} //for

	return strings.TrimSpace(buff.String()), nil
} //parseLine

// NewBooks creates a new list of books using any starting books provided.
func NewBooks(starting ...string) *Books {
	// Check if starting books were provided
	if len(starting) == 0 {
		return &Books{}
	} //if

	return &Books{
		books: starting,
	}
} //NewBookds

// Len returns the number of books.
func (b Books) Len() int {
	return len(b.books)
} //Len

func (b Books) indexOf(book string) int {
	// Loop through all the books
	for i, title := range b.books {
		if title == book {
			return i
		} //if
	} //for

	return -1
} //indexOf

// Has checks if the book is already owned.
func (b Books) Has(book string) bool {
	// Loop through all the books
	for _, title := range b.books {
		if title == book {
			return true
		} //if
	} //for

	return b.indexOf(book) != -1
} //Has

// Add a books.
func (b *Books) Add(book string) {
	// Make sure the book doesn't already exit
	if !b.Has(book) {
		b.books = append(b.books, book)
	} //if
} //Add

// Remove a book.
func (b *Books) Remove(book string) {
	idx := b.indexOf(book)

	// Make sure we have the book first
	if idx == -1 {
		return
	} //if

	// Remove the book
	b.books = append(b.books[:idx], b.books[idx+1:]...)
} //Remove

// Save the books!
func (b Books) Save(out io.Writer) error {
	// Loop through the books
	for _, book := range b.books {
		// Write the current book
		_, err := out.Write([]byte(book + "\n"))

		// Check for an error
		if err != nil {
			return err
		} //if
	} //if

	return nil
} //Save
