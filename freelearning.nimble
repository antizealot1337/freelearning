#name = "freelearning"
version       = "0.1.2"
author        = "antizealot1337"
description   = "Retrieves the free eBook name for PaktPub for today"
license       = "MIT"
srcDir        = "src"
bin           = @["freelearning"]

requires "nim >= 0.10.0"
