package freelearning

import (
	"bytes"
	"testing"
)

func TestParseBooks(t *testing.T) {
	// Some book titles
	title1, title2 := "title1", "title2"

	// The source for a buffer/reader
	source := title1 + "\n" + title2 + "\n"

	// Create a buffer
	buff := bytes.NewBufferString(source)

	// Parse books from the source
	if books, err := ParseBooks(buff); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if num := len(books.books); num != 2 {
		t.Errorf("Expected 2 books but found %d", num)
	} //if
} //TestParseBooks

func TestParseLine(t *testing.T) {
	// Some lines
	line1, line2, line3 := "This is a line\n", "This is a line\r\n",
		"This is a line"

	// Create a buffer
	buff := bytes.NewBufferString(line1)

	// Read the line
	if line, err := parseLine(buff); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if line != "This is a line" {
		t.Errorf(`Expected line to be
			"This is a line"
			but was
			"%s"`, line)
	} //if

	// Create a buffer
	buff = bytes.NewBufferString(line2)

	// Read the line
	if line, err := parseLine(buff); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if line != "This is a line" {
		t.Errorf(`Expected line to be
			"This is a line"
			but was
			"%s"`, line)
	} //if

	// Create a buffer
	buff = bytes.NewBufferString(line3)

	// Read the line
	if line, err := parseLine(buff); err == nil {
		t.Error("Expected an EOF error")
	} else if line != "This is a line" {
		t.Errorf(`Expected line to be
			"This is a line"
			but was
			"%s"`, line)
	} //if
} //TestParseLine

func TestNewBooks(t *testing.T) {
	// Create an empty Books
	books := NewBooks()

	if len(books.books) != 0 {
		t.Error("Expected books to be empty")
	} //if

	if books == nil {
		t.Error("Expected books to not be nil")
	} //if
} //TestNewBooks

func TestBooksLen(t *testing.T) {
	// Create some books
	books := NewBooks()

	if s, l := len(books.books), books.Len(); s != l {
		t.Errorf("Expected slice len (%d) to equal Books.Len (%d)", s, l)
	} //if
} //TestBooksLen

func TestBooksIndexOf(t *testing.T) {
	// Some book titles to add
	title1, title2 := "title1", "title2"

	// Create some books
	books := NewBooks(title1)

	// Add a book
	books.Add(title1)

	if idx := books.indexOf(title1); idx != 0 {
		t.Errorf(`Expected index of "%s" to be 0 but was %d`, title1, idx)
	} //if

	if idx := books.indexOf(title2); idx != -1 {
		t.Errorf(`Expected index of "%s" to be -1 but was %d`, title2, idx)
	} //if
} //TestBooksIndexOf

func TestBooksHas(t *testing.T) {
	// Some book titles to add
	title1, title2 := "title1", "title2"

	// Create some books
	books := NewBooks(title1)

	// Check if it has title1
	if !books.Has(title1) {
		t.Errorf(`Expected books to contain "%s"`, title1)
	} //if

	// Check if it has title2
	if books.Has(title2) {
		t.Errorf(`Expected books to not contain "%s"`, title2)
	} //if
} //TestBooksHas

func TestBooksAdd(t *testing.T) {
	// Some book titles to add
	title1 := "title1"

	// Create some books
	books := NewBooks()

	// Add a book
	books.Add(title1)

	// Check the count
	if num := books.Len(); num != 1 {
		t.Fatalf("Expected 1 book but there were %d", num)
	} //if

	// Make sure books contains the new book
	if !books.Has(title1) {
		t.Errorf(`Expected books to contain "%s"`, title1)
	} //if

	// Make sure a "double add" is not permitted
	books.Add(title1)

	// Check the number of books
	if num := books.Len(); num != 1 {
		t.Error("Expected books to prevent a double add")
	} //if
} //TestBooksAdd

func TestBooksRemove(t *testing.T) {
	// Some book titles to add
	title1, title2 := "title1", "title2"

	// Create some books
	books := NewBooks(title1, title2)

	// Remove title2
	books.Remove(title2)

	// Check if it has title1
	if !books.Has(title1) {
		t.Errorf(`Expected books to contain "%s"`, title1)
	} //if

	// Check if it has title2
	if books.Has(title2) {
		t.Errorf(`Expected books to not contain "%s"`, title2)
	} //if

	// Make sure a bad remove is a no-op
	books.Remove("Does not exist")
	if num := books.Len(); num != 1 {
		t.Error("Expected removal of nonexistent book to have no effect")
	} //if
} //TestBooksRemove

func TestBooksSave(t *testing.T) {
	// Some book titles
	title1, title2 := "title1", "title2"

	// Create the expected title
	expected := title1 + "\n" + title2 + "\n"

	// Create a buffer
	var buff bytes.Buffer

	// Create books
	books := NewBooks(title1, title2)

	if err := books.Save(&buff); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if actual := buff.String(); actual != expected {
		t.Errorf(`Expected buffer to contain
			"%s"
			but was
			"%s"`, expected, actual)
	} //if
} //TestBooksSave
