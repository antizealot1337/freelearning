package main

import (
	"os"

	"bitbucket.org/antizealot1337/freelearning"
)

func main() {
	// Get the free eBook today
	if free, _, err := freelearning.GetFreeEBook(); err != nil {
		os.Stderr.WriteString(err.Error() + "\n")
		os.Exit(-1)
	} else {
		os.Stdout.WriteString(free + "\n")
	} //if
} //func
