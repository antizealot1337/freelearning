package main

import (
	"errors"
	"fmt"
	"os"
	"path"
	"strings"

	"bitbucket.org/antizealot1337/freelearning"
)

const (
	usage = `Description: A command to manage the Packtpub library
Usage:
    packtbooks add <title>
    packtbooks delete <title>
    packtbooks check <title>
    packtbooks show

Commands:
    add    Add a book's title
    delete Removes a book by its title
    check  Check if a book's title exists
    show   Show all the titles
`
)

var (
	booksPath = path.Join(os.Getenv("HOME"), ".packtbooks.txt")
)

func main() {
	// Get the number of args
	numArgs := len(os.Args)

	// Check if only there are no args (other than the program name)
	if numArgs == 1 {
		os.Stdout.WriteString(usage)
		os.Exit(0)
	} //if

	// Open the books file
	bookFile, err := openBooksFile(booksPath)
	if err != nil {
		panic(err)
	} //if
	defer bookFile.Close()

	// Parse the books
	books, err := freelearning.ParseBooks(bookFile)
	if err != nil {
		panic(err)
	} //if

	switch command := strings.ToLower(os.Args[1]); command {
	case "add":
		if title, err := getTitle(); err != nil {
			panic(err)
		} else {
			books.Add(title)
		} //if
	case "delete":
		if title, err := getTitle(); err != nil {
			panic(err)
		} else {
			books.Remove(title)
		} //if
	case "check":
		if title, err := getTitle(); err != nil {
			fmt.Printf("\"%s\" is in the library.", title)
			panic(err)
		} else {
			if books.Has(title) {
				fmt.Printf("\"%s\" is not in the library.", title)
			} //if
		} //if
	case "show":
		// Show all the books
		books.Save(os.Stdout)
	default:
		fmt.Fprintf(os.Stderr, "\"%s\" is not a valid command.\n", os.Args[1])
		os.Exit(-1)
	} //if

	// Seek to the beginning of the file
	bookFile.Seek(int64(os.SEEK_SET), 0)

	// Save the books
	if err := books.Save(bookFile); err != nil {
		panic(err)
	} //if
} //main

func openBooksFile(path string) (*os.File, error) {
	// Open the file from the path
	bookfile, err := os.OpenFile(booksPath, os.O_RDWR, 0)
	if err != nil {
		if os.IsNotExist(err) {
			// Create the file
			bookfile, err = os.Create(booksPath)
			if err != nil {
				return nil, err
			} //if

			fmt.Println("Creating file", booksPath)
		} else {
			return nil, err
		} //if
	} //if

	return bookfile, nil
} //openBooksFile

func getTitle() (string, error) {
	// Check for the number of args
	if numArgs := len(os.Args); numArgs < 3 {
		fmt.Printf("Num args %d\n", numArgs)
		return "", errors.New(`Missing "title argument"`)
	} //if

	return os.Args[2], nil
} //getTitle
