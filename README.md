# Freelearning

A command line tool to return the free ebook of the day from
https://www.packtpub.com written in Nim.

# License

Copyright 2018

Licensed under the terms of the MIT license. See License for more details.
